CREATE DATABASE IF NOT EXISTS lp2_pedro_muro;

CREATE TABLE `log`
(
    `id`          int(10)      not null auto_increment,
    `nome`        varchar(100) not null,
    `idade`       int(3)       not null,
    `sexo`        varchar(10)  not null,
    `diagnostico` varchar(100) not null,
    primary key (id)
);
