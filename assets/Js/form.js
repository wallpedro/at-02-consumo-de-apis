class Form {

    static Parse() {
        jQuery('#recomeca').css('display', 'flex');
        var npl = jQuery('#npl').val();
        $("#loading").fadeIn();
        $.ajax({
            method: "POST",
            dataType: 'json',
            url: APPPATH + '/HomeController/AjaxParse',
            data: {
                npl: npl
            },
            success: function (result) {
                if (result == null || result == undefined) {
                    alert('Por favor, nos diga o que você está sentindo');
                }
                if (result.length == 0) {
                    alert('Não pudemos compreendê-lo. Seja mais claro');
                } else {

                    jQuery('#sintomasCard').css('display', 'flex');
                    var count = 1;
                    for (var i in result) {
                        var html = '<tr>';
                        html += '<th scope="row">' + count + '</th>';
                        html += '<th>' + result[i]['name'].toString() + '</th>';
                        html += '<th>' + result[i]['common_name'].toString() + '</th>';
                        html += '<th>' + result[i]['type'].toString() + '</th>';
                        html += '<th>' + result[i]['orth'].toString() + '</th>';
                        html += '</tr>';
                        jQuery('#sintomasList').append(html);
                        count++;
                    }
                    jQuery("[id^='dadosPessoais']").prop('readonly', 'readonly');
                    jQuery("[id^='defaultInline']").prop('readonly', 'readonly');
                    jQuery("[id^='npl']").prop('readonly', 'readonly');
                    jQuery('#nplVerif').prop('disabled', 'true');
                    jQuery('#btnNplSend').removeAttr('disabled');
                }
            },
            error: function (xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
        });
        $("#loading").fadeOut();
    }
}

jQuery('document').ready(function () {
    jQuery('#btnNplSend').prop('disabled', 'true');
    jQuery('#btnSintomaSend').prop('disabled', 'true');
    jQuery('#sendResposta').prop('disabled', 'true');
});

jQuery("[name^='questionAnswer']").click(function () {
    jQuery('#sendResposta').removeAttr('disabled');
});

jQuery('#btnNplSend').click(function () {
    var flag = false;
    jQuery("[id^='dadosPessoais']").each(function () {
        if (this.value == '' || this.value == null || this.value == 'false') {
            flag = true;
        }
    });
    if(flag){
        alert('Por favor, preencha todos os campos');
        location.reload();
    }else{
        jQuery('#form').submit();
    }
});

var form = new Form();


