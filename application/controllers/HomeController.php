<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

    public function index() {
        $this->load->view('common/header.php');
        $this->load->view('common/navbar.php');

        $dados['content'] = $this->load->view('alunoDesc.php', '', true);
        $this->load->view('layout.php', $dados);

        $this->load->view('common/footer.php');
    }


    public function api() {

        $this->load->model('Infermedica/InfermedicaModel', 'model');

        $this->load->view('common/header.php');
        $this->load->view('common/navbar.php');
        $this->load->view('Infermedica/loading.php');

        if ($this->input->post('finished')) {
            $dadosPessoais = json_decode($_COOKIE['dadosPessoais'], true);
            $diagnostic = json_decode($_COOKIE['diagnostic'], true);

            $dados = [
                'nome' => $dadosPessoais['nome'],
                'idade' => $dadosPessoais['age'],
                'sexo' => $dadosPessoais['sex'],
                'diagnostico' => $diagnostic[1]
            ];
            $this->model->insertInteracao($dados);
            unset($_COOKIE['diagCookie']);
            unset($_COOKIE['dadosPessoais']);
            unset($_COOKIE['diagnostic']);
            unset($_COOKIE['questionEvidence']);
            unset($_POST);
        }

        if ($this->input->post('npl')) {
            $parseResult = $this->model->Parse();
            $evidences = $this->model->getEvidences($parseResult['mentions']);
        }

        $dadosPessoais = $this->input->post('dadosPessoais');

        //Primeira requisição ao /diagnosis
        if ($dadosPessoais && $parseResult) {
            set_cookie('dadosPessoais', json_encode($dadosPessoais), time() + 3600);
            $diagFormattedArray = $this->model->formataArray(array_merge($dadosPessoais, ['evidences' => $evidences]));
            $diagReturn = $this->model->Diagnosis($diagFormattedArray);
        }

        //proximas requisições ao diagnosis
        if ($this->input->post('questionAnswer')) {
            $diagCookieArr = json_decode($_COOKIE['diagCookie'], true);
            $questionEvidenceArr = json_decode($_COOKIE['questionEvidence'], true);
            array_push($diagCookieArr['evidence'], ['id' => $questionEvidenceArr['id'], 'choice_id' => $this->input->post('questionAnswer')]);
            $diagReturn = $this->model->Diagnosis($diagCookieArr);
        }

        //Se a requisição obteve êxito
        if (isset($diagReturn)) {
            if ($diagReturn) {
                $questionEvidence = isset($diagReturn['question']['items'][0]) ? $diagReturn['question']['items'][0] : '';
                $jsonDiag = json_encode(isset($diagFormattedArray) ? $diagFormattedArray : $diagCookieArr);
                $jsonQuestion = json_encode(isset($questionEvidence) ? $questionEvidence : $questionEvidenceArr);
                set_cookie('questionEvidence', $jsonQuestion, time() + 3600);
                set_cookie('diagCookie', $jsonDiag, time() + 3600);
            }
        }

        //se há alguma condição com probabilidade maior do que 95% (condicao de parada da entrevista)
        if (isset($diagReturn['conditions'][0]['probability']) && $diagReturn['conditions'][0]['probability'] > 0.95) {
            $diagnostic = [
                $diagReturn['conditions'][0]['id'],
                $diagReturn['conditions'][0]['name']
            ];
            set_cookie('diagnostic', json_encode($diagnostic), time() + 3600);
            $diagReturn = $this->model->Diagnosis($diagCookieArr, true);
            $diagReturn['finished'] = true;
        }

        $dados['form'] = $this->model->getHTML(isset($diagReturn) ? $diagReturn : null);
        $this->load->view('Infermedica/layout.php', $dados);
        $this->load->view('common/footer.php');
    }

    public function log() {
        $this->load->view('common/header.php');
        $this->load->view('common/navbar.php');
        $this->load->model('Infermedica/InfermedicaModel', 'model');

        $registros = $this->model->getLog();

        $dados['form'] = $this->model->getHTML($registros, true);
        $this->load->view('Infermedica/layout.php', $dados);
        $this->load->view('common/footer.php');
    }

    public function AjaxParse() {
        if (isset($_POST['npl'])) {
            $this->load->model('Infermedica/InfermedicaModel', 'model');
            $result = $this->model->Parse($this->input->post('npl'));
        }
        echo json_encode($result['mentions']);
    }
}
