<div class="card" style="max-width: 500px">
    <img class="card-img-top" src="<?= base_url('assets/img/perfil.jpg')?>" alt="Card image cap" style="max-height: auto">
    <div class="card-body">
        <h4 class="card-title"><a>Pedro Henrique Muro Costa</a></h4>
        <p class="card-text"><span style="font-weight: bold">Prontuário: </span>GU3001679</p>
        <a href="<?=base_url('/HomeController/api')?>" class="btn btn-primary">Ir para Aplicação</a>
    </div>
</div>