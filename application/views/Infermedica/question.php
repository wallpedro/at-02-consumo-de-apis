<div class="container mt-5">
    <div class="card">

        <div class="card-header">
            <h3>Pergunta</h3>
        </div>
        <div class="card-body">
            <form method="POST">
                <div class="row">
                    <div class="col-md-12">
                        <div class="list-group mt-2">
                            <p class="list-group-item list-group-item-action active"><?= $question ?></p>
                            <div class="list-group mt-4">
                                <div class="custom-control custom-radio mt-2">
                                    <input type="radio" class="custom-control-input" id="defaultGroupExample1"
                                           name="questionAnswer"
                                           value="<?= $optId[0] ?>">
                                    <label class="custom-control-label"
                                           for="defaultGroupExample1"><?= $optLabel[0] ?></label>
                                </div>

                                <div class="custom-control custom-radio mt-2">
                                    <input type="radio" class="custom-control-input" id="defaultGroupExample2"
                                           name="questionAnswer"
                                           value="<?= $optId[1] ?>">
                                    <label class="custom-control-label"
                                           for="defaultGroupExample2"><?= $optLabel[1] ?></label>
                                </div>

                                <div class="custom-control custom-radio mt-2">
                                    <input type="radio" class="custom-control-input" id="defaultGroupExample3"
                                           name="questionAnswer"
                                           value="<?= $optId[2] ?>">
                                    <label class="custom-control-label"
                                           for="defaultGroupExample3"><?= $optLabel[2] ?></label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <button id="sendResposta" class="btn btn-primary btn-md">Enviar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>