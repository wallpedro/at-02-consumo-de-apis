<div class="card mt-5" id="sintomasCard">
    <div class="card-header">
        <h2>Sintomas</h2>
    </div>
    <div class="card-body">
        <div class="row">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Nome popular</th>
                    <th scope="col">Probabilidade</th>
                </tr>
                </thead>
                <tbody id="sintomasList">
                    <?= isset($row) ? $row : ''; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>