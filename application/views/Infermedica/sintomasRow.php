<th><?= $number ?></th>
<th><?= $name ?></th>
<?= isset($common_name) ? '<th>'.$common_name.'</th>' : '' ?>
<?= isset($risk) ? '<th>'.$risk.'</th>' : '' ?>
<?= isset($prob) && isset($color) ?
    '<th>
    <div class="progress">
        <div class="progress-bar bg-'.$color.'" role="progressbar" style="width:'.$prob.'%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
</th>' : '' ?>

