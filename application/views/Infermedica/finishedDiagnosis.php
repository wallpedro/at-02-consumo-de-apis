<div class="card mt-5" id="sintomasCard">
    <div class="card-header">
        <h2>Diagnóstico Finalizado</h2>
    </div>
    <div class="card-body">
        <div class="card-header text-center bg-<?= $bgColor ?>">
            <h2> <?= $triageLevel ?> </h2>
        </div>
        <form method="post">
            <div class="row">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Emergência?</th>
                    </tr>
                    </thead>
                    <tbody id="sintomasList">
                    <?= isset($row) ? $row : ''; ?>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <input hidden id="finished" name="finished" value="true">
                <button class="btn btn-primary btn-md">Continuar</button>
            </div>
        </form>
    </div>
</div>
