<div class="card mt-5" id="log">
    <div class="card-header">
        <h2>Registro de Utilização</h2>
    </div>
    <div class="card-body">
        <div class="row">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Idade</th>
                    <th scope="col">Sexo</th>
                    <th scope="col">Diagnóstico</th>
                </tr>
                </thead>
                <tbody id="sintomasList">
                <?= isset($register) ? $register : ''; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
