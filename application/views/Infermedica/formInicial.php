<div class="container mt-5">
    <div class="card">
        <div class="card-header">
            <h1>Dados Pessoais</h1>
        </div>
        <div class="card-body">
            <form method="POST" id="form" class="needs-validation" novalidate>
                <div class="form-row">
                    <div class="col-md-9">
                        <div class="md-form form-group">
                            <input type="text" class="form-control" id="dadosPessoais[nome]" name="dadosPessoais[nome]"
                                   value="<?= set_value('dadosPessoais[nome]') ?>"
                                   placeholder="Nome" required>
                            <label for="dadosPessoais[nome]">Nome</label>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="md-form form-group">
                            <input type="number" class="form-control" id="dadosPessoais[age]"
                                   name="dadosPessoais[age]" value="<?= set_value('dadosPessoais[age]') ?>"
                                   placeholder="Idade" required>
                            <label for="dadosPessoais[age]">Idade</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="input-group col-md-6">
                        <select class="browser-default custom-select custom-select-md" id="dadosPessoais[sex]"
                                name="dadosPessoais[sex]" required>
                            <option disabled selected value="false">Sexo...</option>
                            <option value="male">Masculino</option>
                            <option value="female">Feminino</option>
                        </select>
                    </div>
                </div>
                <div class="row" id="formParse">
                    <div class="col-md-8">
                        <div class="md-form form-group">
                            <input type="text" class="form-control" id="npl" name="npl" placeholder="Diga o que sente">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-primary btn-md" id="nplVerif" onclick="Form.Parse()">
                            Verificar
                        </button>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-primary btn-md" type="button" id="btnNplSend" disabled="true">Enviar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="card mt-5" style="display: none" id="sintomasCard">
        <div class="card-header">
            <h1>Sintomas</h1>
        </div>
        <div class="card-body">
            <div class="row">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Nome popular</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">Texto</th>
                    </tr>
                    </thead>
                    <tbody id="sintomasList">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


