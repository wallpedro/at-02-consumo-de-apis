<div id="loading"></div>
<style>
    #loading{
        display: none;
        position: fixed;
        z-index: 1000;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(255,255,255,0.75);
        background-image: url("<?= base_url('assets/img/loading.gif') ?>");
        background-repeat: no-repeat;
        background-position: center;
    }
</style>
