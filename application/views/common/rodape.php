<footer class="page-footer font-small blue navbar-fixed-bottom pt-4 mt-5">

    <div class="container-fluid text-center text-md-left">

        <div class="row">

            <div class="col-md-6 mt-md-0 mt-3">
                <h5 class="text-uppercase">Infermédica API</h5>
                <p>Esta é uma aplicação baseada na API Infermédica</p>

            </div>

            <hr class="clearfix w-100 d-md-none pb-3">
        </div>
    </div>
    <div class="footer-copyright text-center py-3">© 2019 Copyright:
        <a href="https://mdbootstrap.com/education/bootstrap/"> Pedro Muro</a>
    </div>
</footer>

