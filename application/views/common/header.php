<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Infermedica API</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link href="<?= base_url('assets/mdb/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/mdb/css/mdb.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/mdb/css/style.css') ?>" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="48x48" href="https://bit.ly/2LVbHJq">
    <script>
        var APPPATH = '<?= base_url(); ?>';
    </script>
</head>

<body style="background: url(<?= base_url('/assets/img/bg.jpg') ?>); background-size: 300px; height: 100%">