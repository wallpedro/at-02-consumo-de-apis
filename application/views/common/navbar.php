<nav class="mb-1 navbar navbar-expand-lg navbar-dark primary-color">
    <a class="navbar-brand" href="#">Infermedica API</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
            aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?=base_url()?>">Página inicial
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=base_url('/HomeController/api')?>">Aplicação
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=base_url('/HomeController/log')?>">Registro de utilização
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item" id="recomeca" style="display: none">
                <a class="nav-link" href="<?=base_url('/HomeController/api')?>">Recomeçar formulário
                    <span class="sr-only">(current)</span>
                </a>
            </li>
        </ul>
    </div>
</nav>
