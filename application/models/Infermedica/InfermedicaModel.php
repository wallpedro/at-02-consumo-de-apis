<?php

include_once APPPATH . '\libraries\Infermedica_API\GeradorHTML.php';

class InfermedicaModel extends CI_Model {
    public function getHTML($data, $log = false) {

        $html = new GeradorHTML();

        if($log){
            $returnHtml = $html->getLogPage($data);
        }elseif ($data) {
            $returnHtml = $html->getDiagForm($data);
        } else {
            $returnHtml = $html->getFormInicial();
        }
        return $returnHtml;
    }

    public function Parse() {

        $this->load->library('Infermedica_API/Operacoes.php', null, 'medica');

        if ($this->input->post('npl')) {
            $result = $this->medica->Parse($this->input->post('npl'));
            return $result;
        }
        return false;
    }

    public function formataArray(Array $dados) {
        $arr['sex'] = $dados['sex'];
        $arr['age'] = $dados['age'];
        foreach ($dados['evidences'] as $key => $evidence) {
            $arr['evidence'][$key]['id'] = $evidence[0];
            $arr['evidence'][$key]['choice_id'] = $evidence[1];
            if (isset($evidence[2])) {
                $arr['evidence'][$key]['initial'] = $evidence[2];
            }
        }
        return $arr;
    }

    public function getEvidences(Array $dados) {
        if (is_array($dados)) {
            foreach ($dados as $sintoma) {
                $evidence[] = $sintoma['id'];
                $evidence[] = $sintoma['choice_id'];
                if (isset($sintoma['initial'])) {
                    $evidence[] = $sintoma['initial'];
                }
                $evidences[] = $evidence;
                unset($evidence);
            }
        }
        return $evidences;
    }

    public function Diagnosis(Array $dados, $isFinished = false) {
        $this->load->library('Infermedica_API/Operacoes.php', null, 'medica');

        $result = $this->medica->Diagnosis($dados, $isFinished);

        if ($result) {
            return $result;
        }
        return false;
    }

    public function insertInteracao($dados){
        $this->db->insert('log', $dados);
    }

    public function getLog(){
        $query = $this->db->get('log');
        return $query->result_array();
    }

}