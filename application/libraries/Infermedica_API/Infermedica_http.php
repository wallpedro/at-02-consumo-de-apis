<?php

require 'vendor/autoload.php';

use GuzzleHttp\Client;


abstract class Infermedica_http
{
    protected $client;

    function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://api.infermedica.com/v2/',
            'verify' => false,
            'headers' => [
                'App-Id' => '28dd1112',
                'App-Key' => 'dee5ee5c5595d33ea970286a4f395270'
            ]
        ]);
    }
}