<?php

require 'vendor/autoload.php';

include_once 'Infermedica_http.php';

use GuzzleHttp\Client;

class Operacoes extends Infermedica_http {
    public function Parse(String $data) {

        $response = $this->client->request('POST', 'parse', [
            'json' => [
                'text' => $data
            ]
        ]);

        $resp = json_decode($response->getBody()->getContents(), true);

        foreach ($resp['mentions'] as $key => $mention) {
            $resp['mentions'][$key] = array_merge($mention, ['initial' => 'true']);
        }

        return $resp;
    }

    public function Diagnosis($data, $isTriage = false) {

        $endpoint = 'diagnosis';
        if ($isTriage) {
            $endpoint = 'triage';
        }

        try {
            $response = $this->client->request('POST', $endpoint, [
                'json' => [
                    'sex' => 'male',
                    'age' => '11',
                    'evidence' => $data['evidence'],
                    'extras' => ['disable_groups' => 'true']
                ]
            ]);
        } catch (Exception $exception) {
            return false;
        }
        $resp = json_decode($response->getBody()->getContents(), true);
        return $resp;
    }


}