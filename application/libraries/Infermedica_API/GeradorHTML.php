<?php


class GeradorHTML extends CI_Object {

    public function getFormInicial($data = null) {
        $html = $this->load->view('Infermedica/formInicial.php', '', true);
        return $html;
    }

    public function getDiagForm($dadosDiag) {

        if (isset($dadosDiag['finished']) && $dadosDiag['finished']) {
            $html = $this->getFinishedDiagPage($dadosDiag);
            return $html;
        }
        $html = '';
        $htmlRows['row'] = '';
        $dadosQuestion['question'] = $dadosDiag['question']['text'];

        foreach ($dadosDiag['question']['items'][0]['choices'] as $key => $escolha) {
            $dadosQuestion['optLabel'][$key] = $escolha['label'];
            $dadosQuestion['optId'][$key] = $escolha['id'];
        }

        foreach ($dadosDiag['conditions'] as $key => $condition) {
            $htmlRows['row'] .= '<tr>';
            $dadosConditions['number'] = $key + 1;
            $dadosConditions['name'] = $condition['name'];
            $dadosConditions['common_name'] = $condition['common_name'];
            $dadosConditions['prob'] = $condition['probability'] * 100;
            $dadosConditions['color'] = $this->setProgressBarColor($dadosConditions['prob']);
            $htmlRows['row'] .= $this->load->view('Infermedica/sintomasRow.php', $dadosConditions, true);
            $htmlRows['row'] .= '</tr>';
        }


        $html .= $this->load->view('Infermedica/question.php', $dadosQuestion, true);
        $html .= $this->load->view('Infermedica/sintomasCard.php', $htmlRows, true);
        return $html;
    }

    private function getFinishedDiagPage($dadosTriage) {
        if ($dadosTriage['triage_level'] == 'emergency') {
            $htmlRows['triageLevel'] = 'Procure um médico imediatamente';
            $htmlRows['bgColor'] = 'danger';
        } elseif ($dadosTriage['triage_level'] == 'consultation') {
            $htmlRows['triageLevel'] = 'Procure um médico assim que possível';
            $htmlRows['bgColor'] = 'info';
        } else {
            $htmlRows['triageLevel'] = 'Não é necessário procurar um médico, apenas se cuide';
            $htmlRows['bgColor'] = 'success';
        }
        $htmlRows['row'] = '';

        foreach ($dadosTriage['serious'] as $key => $condition) {
            $htmlRows['row'] .= '<tr>';
            $dadosConditions['number'] = $key + 1;
            $dadosConditions['name'] = $condition['name'];
            $dadosConditions['risk'] = $condition['is_emergency'] ? 'Sim' : "Não";
            $htmlRows['row'] .= $this->load->view('Infermedica/sintomasRow.php', $dadosConditions, true);
            $htmlRows['row'] .= '</tr>';
        }
        return $this->load->view('Infermedica/finishedDiagnosis.php', $htmlRows, true);
    }

    private function setProgressBarColor($prob) {
        switch ($prob) {
            case $prob <= 25:
                $color = 'success';
                break;
            case $prob <= 75:
                $color = 'info';
                break;
            case $prob <= 90:
                $color = 'warning';
                break;
            default:
                $color = 'danger';
        }
        return $color;
    }

    public function getLogPage($dados) {

        $rows['register'] = '';

        foreach ($dados as $dado) {
            $rows['register'] .= '<tr>';
            $data['id'] = $dado['id'];
            $data['nome'] = $dado['nome'];
            $data['idade'] = $dado['idade'];
            $data['sexo'] = $dado['sexo'];
            $data['diag'] = $dado['diagnostico'];
            $rows['register'] .= $this->load->view('Infermedica/logRow.php', $data, true);
            $rows['register'] .= '<tr>';
        }
        return $this->load->view('Infermedica/log.php', $rows, true);
    }

}